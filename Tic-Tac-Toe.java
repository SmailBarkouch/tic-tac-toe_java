import java.util.Scanner;
import java.util.Arrays;

public class Main {
    public static void main (String [] args) {
        int [][] board = new int [3][3]; // empty board
        int [][] reset = board; // board used to reset board
        Scanner scan = new Scanner (System.in); // used for user input
        String result = "none"; // used to determining game mode as well as endgame
        int round = 0; // used for determining the end of the game as well as player turns
        int [] win1 = {1, 1, 1}; // winning positions
        int [] win2 = {2, 2, 2};
        int choice; // used for numerical value, fed into locate()
        boolean replay = true; // used to restart the game without rerunning the code
        int [] coor = new int [2]; // the points used on the board to set a player move
        System.out.println ("Welcome to multiplayer tic-tac-toe!");
        while (replay == true) {
            while (!(result.equals("Multiplayer")) && !(result.equals("AI"))) { // chooses game mode
                System.out.println ("Choose a mode, either \"Multiplayer\" or \"AI\"!");
                result = scan.nextLine();
            }
            System.out.println ("To choose a point, type a number from 1 to 9, from left to right, up to down!");
            traverse(board);
            if(result.equals("Multiplayer")) { 
                result = "none";
                while (result.equals("none")) { // multiplayer gamemode
                    System.out.println("Player " + ((round % 2) + 1) + " choose your location ");
                    choice = scan.nextInt(); // accepts user input for a value 1-9
                    coor = locate(choice);
                    if (choice <= 9 && choice >= 1 && checkLoc(board, coor)) { // checks if move is valud
                        if(round % 2 == 0) { // chooses what player move it is
                            board [coor[0]][coor[1]] = 1;
                        } else {
                            board [coor[0]][coor[1]] = 2;
                        }
                        traverse(board); // prints out the board
                        round++; // increases round number
                        result = checkWin(board, round, win1, win2); // check if the game is over
                    }
                }
            } else {
                result = "none";
                while (result.equals("none")) { // AI gamemode
                    System.out.println("Player, choose your location ");
                    choice = scan.nextInt(); // player input
                    coor = locate(choice); // turns it into actual coordinates on the board
                    if (choice <= 9 && choice >= 1 && checkLoc(board, coor) == true) { // check if coordinates are valid
                        board [coor[0]][coor[1]] = 1; // marks the board with imput
                        round++; // round increases
                        aiMove(board, coor); // AI makes a move
                        traverse(board);
                        result = checkWin(board, round, win1, win2); // checks for endgame
                    }
                }
            }
            
            if (result.equals("Player1")) { //checks for the endgame
                System.out.println("Player 1 wins!");
            } else if(result.equals("Player2")) {
                System.out.println("Player 2 wins!");
            } else {
                System.out.println("The board is filled up and there are no more moves to be placed");
            }
            System.out.println("Do you wanna play another game, type true or false!");
            replay = scan.nextBoolean(); // used to restart game
            for (int row = 0; row < board.length; row++) { // row of the board
              for (int column = 0; column < board[0].length;     column++) { // column of the board
                board [row][column] = 0;
              }
            }
        }
        System.out.println("This was made by Smail Barkouch, Daniel Gendy, Isar Skulason, and Alex Chawdhry!");
    }
    
    public static boolean checkLoc (int [][] board, int [] coor) // checks if move is already taken or marked
    {
        if (board[coor[0]] [coor[1]] == 1 || board[coor[0]] [coor[1]] == 2 ) // if that position has a listed 1 or 2
            return false;
        return true;
    }
    
    
    public static void aiMove (int [][] board, int [] coor) 
    {
        boolean move = false;
        int choice = 0;
        while (move == false) { // if that move is false, it will keep repeating
            choice = (int)(Math.random() * (9-1)) + 1; // random move 1-9
            coor = locate(choice);
            if(checkLoc(board, coor) == true) // only true if the location is not taken
                move = true;
        }
        board[coor[0]][coor[1]] = 2; // logs valid location
    }
    
    public static void traverse (int [][] arr) // traverses the board and switches 1 and 2 to Xs and Os
    {
        for (int row = 0; row < arr.length; row++) { // row of the board
            for (int column = 0; column < arr[0].length; column++) { // column of the board
                if(column == 0) // determines what point to choose
                    System.out.print("|");
                if(arr[row][column] == 1) {
                    System.out.print("X|");
                } else if(arr[row][column] == 2) {
                    System.out.print("O|");
                } else {
                  System.out.print(" |"); // empty piece
                }

            }
            System.out.println();
        }
        System.out.println();
    }
    
   public static int[] locate (int loc) //method header 
   {
       if(loc == 1) {       //The rest of the method are just locations on the 
           int[] a = {0,0};   // grid. When a player puts in a number 1-9, this 
           return a;          // method finds its location and inputs the X or O 
       } else if(loc == 2) {  // on the grid. 
           int[] a = {0,1};   //Location on the grid
           return a;
       } else if(loc == 3){
           int[] a = {0,2};   //Location on the grid
           return a;
       } else if(loc == 4) {
           int[] a = {1,0};   //Location on the grid
           return a;
       } else if(loc == 5) {
           int[] a = {1,1};   //Location on the grid
           return a;
       } else if(loc == 6) {
           int[] a = {1,2};   //Location on the grid
           return a;
       } else if(loc == 7) {
           int[] a = {2,0};   //Location on the grid
           return a;
       } else if(loc == 8) {
           int[] a = {2,1};   //Location on the grid
           return a;
       } else {
           int[] a = {2,2};   //Location on the grid
           return a;
       }
   }
  
   public static String checkWin (int [][] board, int round, int [] win1, int [] win2)   //Method header needs parameters
   {   
        if (round == 8)     //If it reaches round 8 and there is no winner, the the output is tie.
           return "tie";            
        if (round < 3)      //There can’t be a winner if only two rounds are played.
           return "none";    
          
        int [] column1 = {board[0][0],board[1][0],board[2][0]}; //The next five lines declare where players can possibly win.
        int [] column2 = {board[0][1],board[1][1],board[2][2]};  
        int [] column3 = {board[0][2],board[1][2],board[2][2]};  
        int [] diagnol1 = {board[0][0],board[1][1],board[2][2]}; 
        int [] diagnol2 = {board[0][2],board[1][1],board[2][0]}; 
        if(Arrays.equals(board[0], win1) || Arrays.equals(board[1], win1) || Arrays.equals(board[2], win1) || Arrays.equals(column1, win1) || Arrays.equals(column2, win1) || Arrays.equals(column3, win1) || Arrays.equals(diagnol1, win1) || Arrays.equals(diagnol2, win1))
           return "Player1";
        if(Arrays.equals(board[0], win2) || Arrays.equals(board[1], win2) || Arrays.equals(board[2], win2) || Arrays.equals(column1, win2) || Arrays.equals(column2, win2) || Arrays.equals(column3, win2) || Arrays.equals(diagnol1, win2) || Arrays.equals(diagnol2, win2))
           return "Player2";      //The two if statements in the method tell
        return "none";              //which player has won or lost in any which 
   }                               //way, whether it be vertical, diagonal, or  
//This the end of the method         horizontal. 
}  
//This is the end of the entire program
